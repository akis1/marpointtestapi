﻿using System;
using System.Collections;
using MarpointTestApi.Models;
using Microsoft.EntityFrameworkCore;

namespace MarpointTestApi.Repository
{
    public class EmployeeContext : DbContext
    {
       

        public EmployeeContext(DbContextOptions options)
            : base(options)
        {
        }

        public DbSet<Employee> Employees { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>().HasData(new Employee
            {
                EmployeeId = 1,
                FirstName = "Test1",
                LastName = "Test1Lastname",
                Email = "test1@gmail.com",
                DateOfBirth = new DateTime(1960, 07, 15),
                PhoneNumber = "111-222-3333"

            }, new Employee
            {
                EmployeeId = 2,
                FirstName = "Test2",
                LastName = "Test2Lastname",
                Email = "test2@gmail.com",
                DateOfBirth = new DateTime(1990, 07, 7),
                PhoneNumber = "444-555-6666"
            }, new Employee
            {
                EmployeeId = 3,
                FirstName = "Test3",
                LastName = "Test3Lastname",
                Email = "test3@gmail.com",
                DateOfBirth = new DateTime(1995, 12, 7),
                PhoneNumber = "777-888-9999"
            });
        }

       
    }
}