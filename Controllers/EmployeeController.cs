﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using MarpointTestApi.Models;
using MarpointTestApi.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace MarpointTestApi.Controllers
{
    [Route("api/employees")]
    [Produces("application/json")]
    [ApiController]
    [Authorize]
    public class EmployeeController : ControllerBase
    {
        private readonly EmployeeContext _context;
        private readonly IEmployees<Employee> _dataRepository;
        public EmployeeController(EmployeeContext context, IEmployees<Employee> dataRepository)
        {
            _dataRepository = dataRepository;
            _context = context;
        }

        //get all
        [HttpGet]
        public IEnumerable<Employee> Get()
        {
            return _context.Employees.OrderByDescending(e => e.EmployeeId);
        }
       //Get by id
        [HttpGet("{id}", Name = "Get")]
        public async Task<IActionResult> Get(int id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var employee = await _context.Employees.FindAsync(id);

            if (employee == null)
                return NotFound("The Employee couldn't be found.");

            return Ok(employee);
        }
       
        // create new
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Employee employee)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            _dataRepository.Add(employee);
            var save = await _dataRepository.SaveAsync(employee);
            return CreatedAtAction("Get", new { Id = employee.EmployeeId }, employee);
        }
        //edit
      
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] Employee employee)
        {
           if (!ModelState.IsValid)
                return BadRequest(ModelState);
           if (id != employee.EmployeeId)
               return BadRequest();
           _context.Entry(employee).State = EntityState.Modified;
           try
           {
               var entity = await _context.Employees.FindAsync(id);
               if (entity == null)
               {
                   return BadRequest("Employee with Id " + id + " not found to update");

               }

               entity.FirstName = employee.FirstName;
               entity.LastName = employee.LastName;
               entity.DateOfBirth = employee.DateOfBirth;
               entity.Email = employee.Email;
               entity.PhoneNumber = employee.PhoneNumber;
               await _context.SaveChangesAsync();
               return Ok(entity);
           }
           catch (DbUpdateConcurrencyException)
           {
               if (!EmployeeExists(id))
                   return NotFound();
               throw;
           }

           return NoContent();
        }
       
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            
            var  employee =await _context.Employees.FindAsync(id);
            if (employee == null)
                return NotFound($"The Employee with id:{id} couldn't be found.");

            _dataRepository.Delete(employee);
            var save = await _dataRepository.SaveAsync(employee);

            return Ok(employee);
        }

        private bool EmployeeExists(int id)
        {
            return _context.Employees.Any(e => e.EmployeeId == id);
        }
    }
}
